package Modelo;

public class DefineTabla {

    public DefineTabla(){}

    public static abstract class Usuarios{
        public static  final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_NOMBREUSUARIO = "nombre";
        public static final String COLUMN_NAME_CORREO = "correo";
        public static final String COLUMN_NAME_PASSWORD = "contrasenia";
    }
    public static String[] REGISTROS = new String[]{
            Usuarios.COLUMN_NAME_ID,
            Usuarios.COLUMN_NAME_NOMBREUSUARIO,
            Usuarios.COLUMN_NAME_CORREO,
            Usuarios.COLUMN_NAME_PASSWORD
    };
}