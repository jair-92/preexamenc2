package Modelo;

import com.example.preexamen02.Usuarios;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertUsuario(Usuarios usuario);
}
