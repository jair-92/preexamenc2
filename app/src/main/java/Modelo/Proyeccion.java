package Modelo;

import com.example.preexamen02.Usuarios;

import android.database.Cursor;

import java.util.ArrayList;

public interface Proyeccion {
    public boolean obtenerUser(String correo);
    public boolean getUsuario(String nombre, String contrasenia);
    public ArrayList<Usuarios> allUsuarios();
    public Usuarios readUsuario(Cursor cursor);
}

