package com.example.preexamen02;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import Modelo.UsuarioDb;

public class ListaActivity extends AppCompatActivity {
    private Button btnRegresar;

    private ListView lstUsurios;
    private TextView lblEmpresas;
    private TextView lblUsuarios;
    private UsuarioAdapter adaptador;
    static UsuarioDb usuarioDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        iniciarComponentes();

        usuarioDb = new UsuarioDb(getApplicationContext());
        List<Usuarios> lista = usuarioDb.allUsuarios();
        usuarioDb.openDataBase();

        adaptador = new UsuarioAdapter(ListaActivity.this, lista);
        lstUsurios.setAdapter(adaptador);

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });

    }

    private void regresar() {
        AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Empresas Industriales de México");
        dialogo.setMessage(" ¿Desea regresar a la pagina principal? ");

        dialogo.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        dialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogo.show();
    }

    private void iniciarComponentes() {
        btnRegresar = findViewById(R.id.btnRegresar);
        lstUsurios = findViewById(R.id.listEmpresas);
        lblEmpresas = findViewById(R.id.lblEmpresas);
        lblUsuarios = findViewById(R.id.lblUsuarios);
    }

}

