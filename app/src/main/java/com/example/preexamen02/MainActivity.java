package com.example.preexamen02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import Modelo.UsuarioDb;

public class MainActivity extends AppCompatActivity {
    private EditText txtCorreo;
    private EditText txtPassword;
    private Button btnRegistrar;
    private Button btnIngresar;
    static UsuarioDb usuarioDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCorreo = findViewById(R.id.txtCorreo);
        txtPassword = findViewById(R.id.txtPassword);
        btnRegistrar = findViewById(R.id.btnRegistro);
        btnIngresar = findViewById(R.id.btnIngresar);

        usuarioDb = new UsuarioDb(getApplicationContext());
        usuarioDb.openDataBase();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrar();
            }
        });
    }

    private void registrar() {
        Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
        startActivity(intent);
    }

    private void ingresar() {
        String correo = txtCorreo.getText().toString();
        String password = txtPassword.getText().toString();

        usuarioDb = new UsuarioDb(getApplicationContext());

        boolean siRegsitrado = usuarioDb.getUsuario(correo, password);
        usuarioDb.openDataBase();

        if(siRegsitrado){
            txtCorreo.setText("");
            txtPassword.setText("");
            Intent intent = new Intent(MainActivity.this, ListaActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(),
                    "Correo o Contraseña no válidos", Toast.LENGTH_LONG).show();
        }
    }
}