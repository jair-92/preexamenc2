package com.example.preexamen02;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import Modelo.UsuarioDb;

public class RegistroActivity extends AppCompatActivity {
    private EditText txtNombre;
    private EditText txtCorreo;
    private EditText txtPassword;
    private EditText txtPassword2;

    private TextView lblEmpresas;
    private TextView lblRegistro;

    private Button btnRegistrar;
    private Button btnRegresar;

    private Usuarios usuario;

    private UsuarioDb usuarioDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        txtNombre = findViewById(R.id.txtNombre);
        txtCorreo = findViewById(R.id.txtCorreo);
        txtPassword = findViewById(R.id.txtContrasenia);
        txtPassword2 = findViewById(R.id.txtReContrasenia);
        lblEmpresas = findViewById(R.id.lblEmpresas);
        lblRegistro = findViewById(R.id.lblRegistro);
        btnRegistrar = findViewById(R.id.btnRegistrar);
        btnRegresar = findViewById(R.id.btnRegresar);

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registro();
            }
        });
    }

    private void registro() {
        usuario = new Usuarios();
        usuario.setNombre(txtNombre.getText().toString());
        usuario.setCorreo(txtCorreo.getText().toString());
        usuario.setContrasenia(txtPassword.getText().toString());

        if(validar()){
            if(correosExistentes()){
                Toast.makeText(getApplicationContext(), "Ese correo ya existe ", Toast.LENGTH_SHORT).show();
            }else{
                usuarioDb = new UsuarioDb(getApplicationContext());
                usuarioDb.insertUsuario(usuario);
                Toast.makeText(getApplicationContext(), "Se ha creado con exito ",Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK);
                finish();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Faltan datos ", Toast.LENGTH_SHORT).show();
        }

    }

    private void regresar() {
        AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Empresas Industriales de México");
        dialogo.setMessage(" ¿Seguro que desea regresar? ");

        dialogo.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        dialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogo.show();
    }

    private boolean validar(){
        if (txtNombre.getText().toString().equals("")) return false;
        if (txtCorreo.getText().toString().equals("")) return false;
        if (txtPassword.getText().toString().equals("")) return false;
        if(txtPassword2.getText().toString().equals(txtPassword.getText().toString())) return true;

        return false;
    }

    private boolean correosExistentes(){
        usuarioDb = new UsuarioDb(getApplicationContext());

        boolean correoExistente = usuarioDb.obtenerUser(txtCorreo.getText().toString());
        usuarioDb.openDataBase();

        return correoExistente;
    }

}